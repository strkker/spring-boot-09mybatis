package com.wanxi.springboot09mybatis.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class LoginParams implements Serializable {
    private String username;
    private String password;
    private String wcode;
    private int state;//登录状态
    private String date;//登录时间
}

