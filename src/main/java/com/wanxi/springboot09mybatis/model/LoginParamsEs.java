package com.wanxi.springboot09mybatis.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(indexName = "poems",shards = 1, replicas = 0)
public class LoginParamsEs {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Field(analyzer = "ik_max_word",type = FieldType.Text)
    private String username;
    private String date;
    private String ip;
}
