package com.wanxi.springboot09mybatis.model;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
public class WriteModel extends BaseRowModel {
    @ExcelProperty(value = "英语单词",index = 0)
    private String english;
    @ExcelProperty(value = "汉语意思",index = 1)
    private String chinese;
    @ExcelProperty(value = "时间",index = 2)
    private String time;
    @ExcelProperty(value = "作者",index = 3)
    private String author;

    public String getEnglish() {
        return english;
    }

    public void setEnglish(String english) {
        this.english = english;
    }

    public String getChinese() {
        return chinese;
    }

    public void setChinese(String chinese) {
        this.chinese = chinese;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
}
