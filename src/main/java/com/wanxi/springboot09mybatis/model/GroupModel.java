package com.wanxi.springboot09mybatis.model;

import java.io.Serializable;

public class GroupModel implements Serializable {
    private int id;
    private String code;
    private String groupName;
    private String teamLeaderName;
    private String teamLeaderCode;
    private String coach;
    private String buildTime;
    private String stage;//阶段
    private int countTotl;//小组总人数
    private int olderId;//之前的组长id
    private int nowId;//现在组长的id;

    public int getOlderId() {
        return olderId;
    }

    public void setOlderId(int olderId) {
        this.olderId = olderId;
    }

    public int getNowId() {
        return nowId;
    }

    public void setNowId(int nowId) {
        this.nowId = nowId;
    }

    public int getCountTotl() {
        return countTotl;
    }

    public void setCountTotl(int countTotl) {
        this.countTotl = countTotl;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getTeamLeaderName() {
        return teamLeaderName;
    }

    public void setTeamLeaderName(String teamLeaderName) {
        this.teamLeaderName = teamLeaderName;
    }

    public String getTeamLeaderCode() {
        return teamLeaderCode;
    }

    public void setTeamLeaderCode(String teamLeaderCode) {
        this.teamLeaderCode = teamLeaderCode;
    }

    public String getCoach() {
        return coach;
    }

    public void setCoach(String coach) {
        this.coach = coach;
    }

    public String getBuildTime() {
        return buildTime;
    }

    public void setBuildTime(String buildTime) {
        this.buildTime = buildTime;
    }

    public String getStage() {
        return stage;
    }

    public void setStage(String stage) {
        this.stage = stage;
    }
}
