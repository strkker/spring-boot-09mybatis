package com.wanxi.springboot09mybatis.service;

import cn.hutool.extra.tokenizer.Word;
import com.wanxi.springboot09mybatis.model.WordModel;

import java.util.List;

public interface WordService {
    int saveWord(WordModel wordModel);

    List<WordModel> showSelfWord(String username);

    int deleteWord(Integer id);

    WordModel findWordById(Integer id);

    int editWords(WordModel wordModel);

    List<WordModel> showStuWord(WordModel wordModel);

    int wordLook(Integer id);
}
