package com.wanxi.springboot09mybatis.service.impl;

import com.wanxi.springboot09mybatis.dao.EnglishWordDao;
import com.wanxi.springboot09mybatis.model.EnglishWordModel;
import com.wanxi.springboot09mybatis.service.EnglishWordService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

@Service
@Transactional
public class EnglishWordServiceImpl implements EnglishWordService {
    @Resource
    EnglishWordDao englishWordDao;
    /**
     * 保存单词
     * @param englishWordModel
     * @return
     */
    @Override
    public int saveEnglish(EnglishWordModel englishWordModel) {
        String date= LocalDateTime.now().toString();
        englishWordModel.setTimes(date);
        boolean ishave=isExitEnglishWord(englishWordModel);
        if(ishave){
            return englishWordDao.saveEnglish(englishWordModel);
        }
        else {
            return 0;
        }
    }

    /**
     * 哟用户查找单词
     * @param englishWordModel
     * @return
     */
    @Override
    public List<EnglishWordModel> findEnglishWord(EnglishWordModel englishWordModel) {
        List<EnglishWordModel>list=englishWordDao.findEnglishWord(englishWordModel);
        return list;
    }

    /**
     * 根据id查找英语单词
     * @param id
     * @return
     */
    @Override
    public EnglishWordModel findEnglishWordById(Integer id) {
        return englishWordDao.findEnglishWordById(id);
    }

    /**
     * 用户收藏英语单词
     * @param englishWordModel
     * @return
     */
    @Override
    public int collectEnglishWord(EnglishWordModel englishWordModel) {
        boolean ishave=isHascollectEnglishWord(englishWordModel);
        if(ishave){
            return englishWordDao.collectEnglishWord(englishWordModel);
        }
      else {
          return 0;
        }
    }

    /**
     * 用户点赞添加
     * @param englishWordModel
     * @return
     */
    @Override
    public int englishFabulous(EnglishWordModel englishWordModel) {
        boolean ishas=ishasEnglishFabulous(englishWordModel);
        if(ishas){
            return englishWordDao.englishFabulous(englishWordModel);
        }
        else {
            return 0;
        }
    }

    /**
     * 提升点赞量
     * @param id
     * @return
     */
    @Override
    public int addFabulous(Integer id) {
        return englishWordDao.addFabulous(id);
    }

    /**
     * 收藏英语单词展示
     * @param englishWordModel
     * @return
     */
    @Override
    public List<EnglishWordModel> englishColectionShow(EnglishWordModel englishWordModel) {
        List<EnglishWordModel>list=englishWordDao.englishColectionShow(englishWordModel);
        return list;
    }

    /**
     * 用户取消收藏
     * @param id
     * @return
     */
    @Override
    public int deleteCollect(Integer id) {
        return englishWordDao.deleteCollect(id);
    }

    /**
     * 判断用户是否已经点赞
     * @param englishWordModel
     * @return
     */
    public boolean ishasEnglishFabulous(EnglishWordModel englishWordModel){
        EnglishWordModel englishWordModel1=englishWordDao.ishasEnglishFabulous(englishWordModel);
        return englishWordModel1==null;
    }

    /**
     * 判断用户是否收藏了单词
     * @param englishWordModel
     * @return
     */
    public boolean isHascollectEnglishWord(EnglishWordModel englishWordModel){
        EnglishWordModel englishWordModel1=englishWordDao.isHascollectEnglishWord(englishWordModel);
        return englishWordModel1==null;
    }

    /**
     * 判断是否存在单词
     * @param englishWordModel
     * @return
     */
    public boolean isExitEnglishWord(EnglishWordModel englishWordModel){
        EnglishWordModel englishWordModel1=englishWordDao.isExitEnglishWord(englishWordModel);
        return englishWordModel1==null;
    }
}
