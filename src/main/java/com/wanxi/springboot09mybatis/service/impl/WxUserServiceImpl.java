package com.wanxi.springboot09mybatis.service.impl;


import com.wanxi.springboot09mybatis.comoen.JwtTokenUtil;
import com.wanxi.springboot09mybatis.dao.EsLoginParamsDao;
import com.wanxi.springboot09mybatis.dao.WxUserDao;
import com.wanxi.springboot09mybatis.model.LoginParams;
import com.wanxi.springboot09mybatis.model.LoginParamsEs;
import com.wanxi.springboot09mybatis.model.Permission;
import com.wanxi.springboot09mybatis.model.WxUser;
import com.wanxi.springboot09mybatis.service.WxUserService;
import io.jsonwebtoken.lang.Assert;
import org.aspectj.weaver.NewParentTypeMunger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.data.domain.Page;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import static org.elasticsearch.index.query.QueryBuilders.matchQuery;

@Transactional
@Service
@EnableScheduling//定时器
@Component
@CacheConfig(cacheNames = "user")
public class WxUserServiceImpl implements WxUserService {

    @Autowired
    EsLoginParamsDao esLoginParamsDao;
    @Resource
    WxUserDao wxUserDao;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    JwtTokenUtil jwtTokenUtil;

    /**
     * 每天凌晨1点执行一次删除昨天所有用户登录失败信息
     */
    @Scheduled(cron = "0 0 1 * * ? ")
    public void deleteBeforDayLogin(){
        wxUserDao.deleteBeforDayLogin();
    }

    @Override
    public WxUser getUserByName(String name) {
        List<WxUser> users= wxUserDao.getUserByName(name);
//        Assert.isTrue(users.size()==1,"您输入的账户不存在，或者有多个相同的账户");
        return users.get(0);
    }

  public boolean isAreadLoginThreeFail(String username){
        List<LoginParams>list=wxUserDao.isAreadLoginThreeFail(username);
        return list.size()==3;
  }
    /**
     * 先判断用户名存在的话，要是密码错误，或者成功，再把用户保存到es
     * @param loginPrams
     * @return
     */
    @Override
    public String login(LoginParams loginPrams) {
        String username = loginPrams.getUsername();
//        Assert.notNull(username,"账号必须不能为空");
        String password = loginPrams.getPassword();
//        Assert.notNull(password,"密码必须不能为空");
        WxUser userByName = getUserByName(username);
        boolean isAreadLoginThreeFail=isAreadLoginThreeFail(username);//判断今天是否失败登录三次
        if(!isAreadLoginThreeFail){
            if(userByName!=null){
                boolean matches = passwordEncoder.matches(password, userByName.getPassword());
                if(matches) {
                    return jwtTokenUtil.generateToken(userByName);
                }
                else {
                    String date= LocalDate.now().toString();
                    loginPrams.setDate(date);
                    loginPrams.setState(0);
                    int saveFailedLogin=wxUserDao.saveFailedLogin(loginPrams);//保存登录失败信息到数据库
                    return null;
                }
            }
            else {
                return null;
            }
        }
          else {
              return "0";
        }

    }


    @Override
    public List<Permission> getPermissionsByUserId(Integer id) {
        return wxUserDao.getPermisionByUserId(id);
    }

    /**
     * 查找es中用户名
     * @param admin
     * @param i
     * @param i1
     * @return
     */
    @Override
    public Page<LoginParamsEs> search(String admin, int i, int i1) {
        NativeSearchQuery name = new NativeSearchQueryBuilder()
                .withQuery(matchQuery("username", admin)).build();
        Page<LoginParamsEs> search = esLoginParamsDao.search(name);
        return search;
    }

    /**
     * 注册用户
     * @param wxUser
     * @return
     */
    @CacheEvict(key="'findSome'")
    @Override
    public int addUser(WxUser wxUser) {
        boolean isHaveUser=isHaveUser(wxUser);
        String newPwd=passwordEncoder.encode(wxUser.getPassword());
        wxUser.setPassword(newPwd);
        if(!isHaveUser){
            int result=wxUserDao.addUser(wxUser);
            return result;
        }
        else{
            return 2;
        }
    }

    /**
     * 判断是否存在用户
     * @param wxUser
     * @return
     */
    private boolean isHaveUser(WxUser wxUser) {
        WxUser wxUser1=new WxUser();
        wxUser=wxUserDao.isHaveUser(wxUser);
        return  wxUser!=null;
    }

}

