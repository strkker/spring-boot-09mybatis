package com.wanxi.springboot09mybatis.service.impl;

import com.wanxi.springboot09mybatis.dao.SignDao;
import com.wanxi.springboot09mybatis.model.GroupModel;
import com.wanxi.springboot09mybatis.model.SignModel;
import com.wanxi.springboot09mybatis.model.WxUser;
import com.wanxi.springboot09mybatis.service.SignService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

@Service
@Transactional
public class SignServiceImpl implements SignService {
    @Resource
    SignDao signDao;
    /**
     * 查看所有学员
     * @param wxUser
     * @return
     */
    @Override
    public List<WxUser> findAllUser(WxUser wxUser) {
        List<WxUser>list=signDao.findAllUser(wxUser);
        return list;
    }

    /**
     * 展示小组
     * @return
     */
    @Override
    public List<GroupModel> findgroup() {
        List<GroupModel>list=signDao.findgroup();
        return list;
    }

    /**
     * 根据id查找用户
     * @param id
     * @return
     */
    @Override
    public WxUser findUserById(Integer id) {
        return signDao.findUserById(id);
    }

    /**
     * 根据id修改用户
     * @param wxUser
     * @return
     */
    @Override
    public int signUserEdit(WxUser wxUser) {
        return signDao.signUserEdit(wxUser);
    }

    /**
     * 查找所有小组
     * @param groupModel
     * @return
     */
    @Override
    public List<GroupModel> findAllGroup(GroupModel groupModel) {
        List<GroupModel>list=signDao.findAllGroup(groupModel);
        return list;
    }

    /**
     * 得到小组人数
     * @param code
     * @return
     */
    @Override
    public int getCount(String code) {
        return signDao.getCount(code);
    }

    /**
     * 查找小组长以及成员
     * @return
     * @param code
     */
    @Override
    public List<WxUser> findLeader(String code) {
        List<WxUser>list=signDao.findLeader(code);
        return list;
    }

    /**
     * 查找所有教练
     * @return
     */
    @Override
    public List<WxUser> findCoach() {
        List<WxUser>list=signDao.findCoach();
        return list;
    }

    /**
     * 根据id查找小组
     * @param id
     * @return
     */
    @Override
    public GroupModel findGroupsById(Integer id) {
        return signDao.findGroupsById(id);
    }

    /**
     * 查找用户code
     * @param username
     * @return
     */
    @Override
    public WxUser findUserCode(String username) {
        return signDao.findUserCode(username);
    }

    /**
     * 修改小组信息
     * @param groupModel
     * @return
     */
    @Override
    public int groupEdits(GroupModel groupModel) {
        return signDao.groupEdits(groupModel);
    }

    /**
     * 查找当前组长id
     * @param teamLeaderCode
     * @return
     */
    @Override
    public WxUser findnowId(String teamLeaderCode) {
        return signDao.findnowId(teamLeaderCode);
    }

    /**
     * 查找老组长id
     * @param teamLeaderCode
     * @return
     */
    @Override
    public WxUser findUseroldId(String teamLeaderCode) {
        return signDao.findUseroldId(teamLeaderCode);
    }

    /**
     * 根据名称查找用户
     * @param username
     * @return
     */
    @Override
    public WxUser findUserByName(String username) {
        return signDao.findUserByName(username);
    }

    /**
     * 添加小组
     * @param groupModel
     * @return
     */
    @Override
    public int addGroup(GroupModel groupModel) {
        boolean isExist=isExistGroup(groupModel);
        String date= LocalDateTime.now().toString() ;
        groupModel.setBuildTime(date);
        if(isExist){
            return signDao.addGroup(groupModel);
        }
        else {
            return 2;
        }
    }

    /**
     * 查找成员没分组的
     * @return
     */
    @Override
    public List<WxUser> findLeaderNoGroup() {
        return signDao.findLeaderNoGroup();
    }

    /**
     * 查找小组长的小组code
     * @param username
     * @return
     */
    @Override
    public WxUser findLeaderCode(String username) {
        return signDao.findLeaderCode(username);
    }
    /**
     * 根据小组code查找小组成员
     * @param groupCode
     * @return
     */
    @Override
    public List<WxUser> findGroupUser(String groupCode) {
        return signDao.findGroupUser(groupCode);
    }
    /**
     * 保存签到人员
     * @param signModel
     * @return
     */
    @Override
    public int saveSign(SignModel signModel) {
        boolean jsSaveSign=isSaveSign(signModel);
        if(jsSaveSign){
            if(signModel.getArrive().equals("")){
                signModel.setArrive(null);
            }            return signDao.saveSign(signModel);
        }
       else {
           return 2;
        }
    }

    /**
     * 展示签到数据
     * @param signModel
     * @return
     */
    @Override
    public List<SignModel> signShow(SignModel signModel) {
        List<SignModel>list=signDao.signShow(signModel);
        return list;
    }

    /**
     * 批量删除签到数据
     * @param id
     * @return
     */
    @Override
    public int deleteAllSign(int id) {
        return signDao.deleteAllSign(id);
    }

    public boolean isSaveSign(SignModel signModel){
        SignModel signModel1=signDao.isSaveSign(signModel);
        return signModel1==null;
    }

    /**
     * 判断小组是否存在
     * @param groupModel
     * @return
     */
    public boolean isExistGroup(GroupModel groupModel){
        GroupModel groupModel1=signDao.isExistGroup(groupModel);
        return groupModel1==null;
    }

}
