package com.wanxi.springboot09mybatis.service.impl;

import com.wanxi.springboot09mybatis.dao.UserRootDao;
import com.wanxi.springboot09mybatis.model.Permission;
import com.wanxi.springboot09mybatis.model.RolePermission;
import com.wanxi.springboot09mybatis.model.UserCharacter;
import com.wanxi.springboot09mybatis.model.WxUser;
import com.wanxi.springboot09mybatis.service.RedisSerice;
import com.wanxi.springboot09mybatis.service.UserRootService;
import com.wanxi.springboot09mybatis.tool.WordTool;
import com.wanxi.springboot09mybatis.tool.WordToolImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Transactional
@Service
@CacheConfig(cacheNames = "user")
public class UserRootServiceImpl implements UserRootService {
    @Resource
    UserRootDao userRootDao;
    @Autowired
    private RedisSerice redisSerice;
    /**
     * 冻结解冻账号
     * @param id
     * @return
     */
    @Override
    @CacheEvict(key="'findSome'")
    public int frozenUser(Integer id ,Integer status) {
        return userRootDao.frozenUser(id,status);
    }

    /**
     * 查找所有用户
     * @param wxUser
     * @return
     */
    @Override
//    @CacheEvict(key="'findSome'")
    @Cacheable(keyGenerator = "userkeyGenerator")
    public List<WxUser> findSome(WxUser wxUser) {
        List<WxUser>list=userRootDao.findSome(wxUser);
        return list;
    }

    /**
     * 管理员删除用户
     * @param id
     * @return
     */

    @CacheEvict(key="'findSome'")
    @Override
    public int rootDeleteUser(Integer id) {
        return userRootDao.rootDeleteUser(id);
    }

    /**
     * 根据id查找用户
     * @param id
     * @return
     */
    @Override
    public WxUser findCharacterById(Integer id) {
        return userRootDao.findCharacterById(id);
    }

    /**
     * 查找所有角色类型
     * @return
     */
    @Override
    public List<UserCharacter> findCharacter() {
        List<UserCharacter>list=userRootDao.findCharacter();
        return list;
    }

    /**
     * 给复选框赋值
     * @param id
     * @return
     */
    @Override
    public List<UserCharacter> setListCharacter(Integer id) {
        List<UserCharacter>list=userRootDao.setListCharacter(id);
        return list;
    }

    /**
     * 删除原有的用户对应的角色
     * @param userId
     */
    @Override
    public void rootDeleteUserRole(int userId) {

        userRootDao.rootDeleteUserRole(userId);
    }

    /**
     * 为用户添加角色
     * @param roleId
     * @return
     */
    @Override
    @CacheEvict(key="'findSome'")
    public int addUserRole(int roleId, int userId) {
        return userRootDao.addUserRole(roleId,userId);
    }

    /**
     * 展示角色与权限的关系
     * @param userCharacter
     * @return
     */
    @Override
//    @CacheEvict(key="'listCharacter'")
    @Cacheable(keyGenerator = "userkeyGenerator")
    public List<UserCharacter> listCharacter(UserCharacter userCharacter) {
        List<UserCharacter>list=userRootDao.listCharacter(userCharacter);
        return list;
    }

    /**
     * 展示角色所有的权限
     * @param
     * @return
     */
    @Override
    public List<Permission> listAuthoritie() {
        List<Permission>list =userRootDao.listAuthoritie();
        return list;
    }

    /**
     * 展示角色信息
     * @param id
     * @return
     */
    @Override
    public UserCharacter setAuthroitie(Integer id) {
        UserCharacter userCharacter=userRootDao.setAuthroitie(id);
        return userCharacter;
    }

    /**
     * 给复选框赋值
     * @param id
     * @return
     */
    @Override
    public List<RolePermission> setAuthrotitieValue(Integer id) {
        List<RolePermission>list=userRootDao.setAuthrotitieValue(id);
        return list;
    }

    /**
     * 删除角色权限
     * @param roleId
     * @return
     */
    @Override
    @CacheEvict(key="'listCharacter'")
    public int deleteAuthroitie(int roleId) {
        int result=userRootDao.deleteAuthroitie(roleId);
        return result;
    }

    /**
     * 给角色添加权限
     * @param roleId
     * @param permissionId
     * @return
     */
    @CacheEvict(key="'listCharacter'")
    @Override
    public int insertAuthroitie(int roleId, int permissionId) {

        return userRootDao.insertAuthroitie(roleId,permissionId);
    }


}
