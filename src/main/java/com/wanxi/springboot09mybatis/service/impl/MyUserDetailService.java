package com.wanxi.springboot09mybatis.service.impl;

import com.wanxi.springboot09mybatis.model.Permission;
import com.wanxi.springboot09mybatis.model.WxUser;
import com.wanxi.springboot09mybatis.service.WxUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;

/**
 * 加载通过用户name获取id来得到用户权限Permission
 */
@Transactional
@Service("userDetailsService")
public class MyUserDetailService implements UserDetailsService {

    @Autowired
    WxUserService wxUserService;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        WxUser user= wxUserService.getUserByName(username);//得到用户
        List<Permission> permissionList= wxUserService.getPermissionsByUserId(user.getId());//通过得到的用户得到id，在通过id查询到权限
        HashSet<Permission> permissions = new HashSet<>(permissionList);
        user.setAuthorities(permissions);
        return user;
    }
}

