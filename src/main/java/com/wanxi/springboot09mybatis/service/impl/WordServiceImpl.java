package com.wanxi.springboot09mybatis.service.impl;

import cn.hutool.extra.tokenizer.Word;
import com.wanxi.springboot09mybatis.dao.WordDao;
import com.wanxi.springboot09mybatis.model.WordModel;
import com.wanxi.springboot09mybatis.service.WordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

@Service
@Transactional
public class WordServiceImpl implements WordService {
    @Resource
    WordDao wordDao;
    /**
     * 保存学院的word内容
     * @param wordModel
     * @return
     */
    @Override
    public int saveWord(WordModel wordModel) {
        String date= LocalDateTime.now().toString();
        wordModel.setTime(date);
        return wordDao.saveWord(wordModel);
    }

    /**
     * 展示学员自身word内容
     * @param username
     * @return
     */
    @Override
    public List<WordModel> showSelfWord(String username) {
        List<WordModel>list=wordDao.showSelfWord(username);
        return list;
    }

    /**
     * 根据id删除word
     * @param id
     * @return
     */
    @Override
    public int deleteWord(Integer id) {
        return wordDao.deleteWord(id);
    }

    /**
     * 根据id查找word
     * @param id
     * @return
     */
    @Override
    public WordModel findWordById(Integer id) {
        WordModel wordModel=wordDao.findWordById(id);
        return wordModel;
    }

    /**
     * 学员修改word
     * @param wordModel
     * @return
     */
    @Override
    public int editWords(WordModel wordModel) {
        String date=LocalDateTime.now().toString();
        wordModel.setTime(date);
        return wordDao.editWords(wordModel);
    }

    /**
     * 展示学员日报
     * @param wordModel
     * @return
     */
    @Override
    public List<WordModel> showStuWord(WordModel wordModel) {
        List<WordModel>list=wordDao.showStuWord(wordModel);
        return list;
    }

    /**
     * 教练已查看word
     * @param id
     * @return
     */
    @Override
    public int wordLook(Integer id) {
        return wordDao.wordLook(id);
    }
}
