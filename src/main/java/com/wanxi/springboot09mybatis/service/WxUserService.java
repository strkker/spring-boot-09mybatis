package com.wanxi.springboot09mybatis.service;

import com.wanxi.springboot09mybatis.model.LoginParams;
import com.wanxi.springboot09mybatis.model.LoginParamsEs;
import com.wanxi.springboot09mybatis.model.Permission;
import com.wanxi.springboot09mybatis.model.WxUser;
import org.springframework.data.domain.Page;

import java.util.List;

public interface WxUserService {

    WxUser getUserByName(String name);
    String login(LoginParams loginPrams);
    List<Permission> getPermissionsByUserId(Integer id);
    Page<LoginParamsEs> search(String username, int i, int i1);
    int addUser(WxUser wxUser);
}

