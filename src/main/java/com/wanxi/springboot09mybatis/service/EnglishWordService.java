package com.wanxi.springboot09mybatis.service;

import com.wanxi.springboot09mybatis.model.EnglishWordModel;

import java.util.List;

public interface EnglishWordService {
    int saveEnglish(EnglishWordModel englishWordModel);

    List<EnglishWordModel> findEnglishWord(EnglishWordModel englishWordModel);

    EnglishWordModel findEnglishWordById(Integer id);

    int collectEnglishWord(EnglishWordModel englishWordModel);

    int englishFabulous(EnglishWordModel englishWordModel);

    int addFabulous(Integer id);

    List<EnglishWordModel> englishColectionShow(EnglishWordModel englishWordModel);

    int deleteCollect(Integer id);
}
