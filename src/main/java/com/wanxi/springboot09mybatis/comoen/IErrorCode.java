package com.wanxi.springboot09mybatis.comoen;

public interface IErrorCode {

    long getCode();

    String getMessage();
}
