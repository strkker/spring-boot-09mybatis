package com.wanxi.springboot09mybatis.config;

import com.wanxi.springboot09mybatis.dao.EsLoginParamsDao;
import com.wanxi.springboot09mybatis.model.LoginParamsEs;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.logging.Logger;

@Aspect
@Component
public class WebLogAspect {

    @Resource
    EsLoginParamsDao esLoginParamsDao;

    private Logger logger = Logger.getLogger(String.valueOf(getClass()));

    @Pointcut("execution(public * com.wanxi.springboot09mybatis.service.WxUserService.login(..))")//切面
    public void webLog(){

    }

    @Before("webLog()")
    public void doBefore(JoinPoint joinPoint) throws Throwable {
        // 接收到请求，记录请求内容
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        // 记录下请求内容
        logger.info("URL : " + request.getRequestURL().toString());
        logger.info("HTTP_METHOD : " + request.getMethod());
        logger.info("IP : " + request.getRemoteAddr());
        logger.info("CLASS_METHOD : " + joinPoint.getSignature().getDeclaringTypeName() + "." + joinPoint.getSignature().getName());
        logger.info("ARGS : " + Arrays.toString(joinPoint.getArgs()));
        String alls=Arrays.toString(joinPoint.getArgs());
        String arr[]=alls.split(",");
        //得到登录的用户名
        String name=arr[0];
        int index=name.indexOf("=");
        String names=name.substring(index+1);//用户名
        String ip=request.getRemoteAddr();//用户登录ip地址
        String date= LocalDateTime.now().toString();//用户登录时间
        imporoUserToEs(names,ip,date);//把用户登录信息保存在elasticsearch中
    }
    @AfterReturning(returning = "ret", pointcut = "webLog()")
    public void doAfterReturning(Object ret) throws Throwable {
        // 处理完请求，返回内容
        logger.info("RESPONSE : " + ret);
    }

    private LoginParamsEs imporoUserToEs(String name,String ip,String date) {
        LoginParamsEs loginParamsEs=new LoginParamsEs();
        loginParamsEs.setUsername(name);
        loginParamsEs.setIp(ip);
        loginParamsEs.setDate(date);
        LoginParamsEs save = esLoginParamsDao.save(loginParamsEs);
        return save;
    }

}

