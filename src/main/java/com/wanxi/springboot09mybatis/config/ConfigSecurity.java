package com.wanxi.springboot09mybatis.config;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
//import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
//import org.springframework.security.core.authority.AuthorityUtils;
//import org.springframework.security.core.userdetails.User;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//import org.springframework.security.crypto.password.PasswordEncoder;
//import org.springframework.security.provisioning.InMemoryUserDetailsManager;
//
//@EnableWebSecurity
//public class ConfigSecurity  extends WebSecurityConfigurerAdapter {
//    @Override
//    protected void configure(HttpSecurity http) throws Exception {
//        http.authorizeRequests()
//                .antMatchers("/css/**", "/js/**", "/fonts/**").permitAll() //都可以访问
//                .antMatchers("/users/**").hasRole("ADMIN") //需要相应的角色才能访问
//                .anyRequest().authenticated() // 任何请求都需要认证
//                .and()
//                .formLogin() //基于Form表单登录验证
//           ;
//
//        ;
//    }
//
//    /**
//     * 第一种用户存内存
//     * @param auth
//     * @throws Exception
//     */
////    @Autowired
////    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
////        auth.inMemoryAuthentication() //认证信息存储到内存中
////                .passwordEncoder(passwordEncoder())
////                .withUser("user").password(passwordEncoder().encode("123456")).roles("ADMIN")
////                .authorities("wx:product:read","wx:product:delete");
////    }
//
//    /**
//     * 第二种用户存内存
//     * @return
//     * @throws Exception
//     */
//    @Bean
//    public UserDetailsService userDetailsServiceBean() throws Exception {
//        InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager();
//        manager.createUser(User.withUsername("jujiale").password(passwordEncoder().encode("123456")).authorities("success").build());
//        return manager;
//    }
//
//    /**
//     * 密码加密
//     * @return
//     */
//    @Bean
//    public PasswordEncoder passwordEncoder() {
//        return new BCryptPasswordEncoder();
//    }
//
//
//}
