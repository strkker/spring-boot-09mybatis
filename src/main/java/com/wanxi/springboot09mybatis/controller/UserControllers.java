package com.wanxi.springboot09mybatis.controller;

import com.wanxi.springboot09mybatis.comoen.CommonResult;
import com.wanxi.springboot09mybatis.comoen.JwtTokenUtil;
import com.wanxi.springboot09mybatis.model.LoginParams;
import com.wanxi.springboot09mybatis.model.LoginParamsEs;
import com.wanxi.springboot09mybatis.model.WxUser;
import com.wanxi.springboot09mybatis.service.RedisSerice;
import com.wanxi.springboot09mybatis.service.WxUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.elasticsearch.index.reindex.LeaderBulkByScrollTaskState;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.data.domain.Page;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.lang.annotation.ElementType;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;
@Api(tags="用户登录注册")
@RestController
@EnableAutoConfiguration
@RequestMapping("/user")
public class UserControllers {
    @Value("${jwt.tokenHead}")
    String tokenHead;
    @Autowired
    WxUserService wxUserService;
    @Autowired
    JwtTokenUtil jwtTokenUtil;
    @Autowired
    RedisSerice redisSerice;
    @ApiOperation("用户登录")
    @PostMapping("/login")
    public CommonResult login(@RequestBody LoginParams loginParams, HttpServletRequest request){
            HttpSession session=request.getSession();
            String code=(String)session.getAttribute("randomcode_key");
            if(code.equals(loginParams.getWcode())){
                HashMap<String, String> data = new HashMap<>();
                String token = null;
                try {
                    token = wxUserService.login(loginParams);//在generateToken创建一个token对象，保存了创建时间，用户名字
                } catch (Exception e) {
                    e.printStackTrace();
                    return CommonResult.validateFailed("用户名或密码错误");
                }
                if (StringUtils.isEmpty(token)){
                    return CommonResult.validateFailed("用户名或密码错误");
                }
                if(token.equals("0")){
                    return CommonResult.failed("今天已经错误登录三次账号已被冻结");
                }
                String username = jwtTokenUtil.getUserNameFromToken(token);
                data.put("tokenHead",tokenHead);
                data.put("access_token",token);
                data.put("users", username);
                // localStorage.setItem("Authorization","Bearer sdsdfdfds")
                // $ajax{data:{},type:"",header:{"Authorization":"Bearer sdsdfdfds"}}
                return CommonResult.success(data);
            }
         else {
             return CommonResult.unauthorized("3");
            }

    }

    /**
     * 根据用户命去es中查找用户
     * @param username
     * @return
     */
    @ApiOperation("验证es保存用户登录信息")
    @GetMapping("/findAll")
    public Page<LoginParamsEs> findAll(@RequestParam String username){
        Page<LoginParamsEs> d = wxUserService.search(username, 1, 10);
        return d;
    }

    /**
     * 注册用户
     * @param wxUser
     * @return
     */
    @ApiOperation("注册用户")
    @PostMapping("/addUser")
    public CommonResult addUser(@RequestBody WxUser wxUser){
           wxUser.setGroupCode("no");
           String date=LocalDateTime.now().toString();
           wxUser.setCreatTime(date);
           wxUser.setStatus(1);
           int result=wxUserService.addUser(wxUser);
           return CommonResult.success(result);
    }
    
    //todo  注册
    //1 接受账号密码
    //2 查询是有存在重复账号
    //3 密码加密
    //4 保存到数据库

}

