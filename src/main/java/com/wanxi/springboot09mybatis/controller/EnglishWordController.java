package com.wanxi.springboot09mybatis.controller;

import com.wanxi.springboot09mybatis.comoen.CommonResult;
import com.wanxi.springboot09mybatis.model.EnglishWordModel;
import com.wanxi.springboot09mybatis.model.WriteModel;
import com.wanxi.springboot09mybatis.service.EnglishWordService;
import com.wanxi.springboot09mybatis.tool.ExcelToole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/englishWord")
public class EnglishWordController {

    @Autowired
    EnglishWordService englishWordService;

    /**
     * 提交单词
     * @param englishWordModel
     * @return
     */
    @PostMapping("/saveEnglish")
    @PreAuthorize("hasAuthority('wanxi:user:common')")
    public CommonResult saveEnglish(@RequestBody EnglishWordModel englishWordModel){
        int result=englishWordService.saveEnglish(englishWordModel);
        return CommonResult.success(result);
    }

    /**
     * 查找单词
     * @param englishWordModel
     * @return
     */
    @PostMapping("/findEnglishWord")
    @PreAuthorize("hasAuthority('wanxi:user:common')")
    public CommonResult findEnglishWord(@RequestBody EnglishWordModel englishWordModel){
        List<EnglishWordModel>list=englishWordService.findEnglishWord(englishWordModel);
        return CommonResult.success(list);
    }

    /**
     * 根据id查找单词
     * @param id
     * @return
     */
    @RequestMapping("/findEnglishWordById")
    @PreAuthorize("hasAuthority('wanxi:user:common')")
    public CommonResult findEnglishWordById(@RequestParam Integer id){
        EnglishWordModel englishWordModel=englishWordService.findEnglishWordById(id);
        return CommonResult.success(englishWordModel);
    }

    /**
     * 用户收藏英语单词
     * @param englishWordModel
     * @return
     */
    @PostMapping("/collectEnglishWord")
    @PreAuthorize("hasAuthority('wanxi:user:common')")
    public CommonResult collectEnglishWord(@RequestBody EnglishWordModel englishWordModel){
        int result=englishWordService.collectEnglishWord(englishWordModel);
        return CommonResult.success(result);
    }

    /**
     * 用户点赞添加
     * @param englishWordModel
     * @return
     */
    @PostMapping("/englishFabulous")
    @PreAuthorize("hasAuthority('wanxi:user:common')")
    public CommonResult englishFabulous(@RequestBody EnglishWordModel englishWordModel){
        int result=englishWordService.englishFabulous(englishWordModel);
        return CommonResult.success(result);
    }

    /**
     * 提升点赞量
     * @param id
     * @return
     */
    @PostMapping("/addFabulous")
    @PreAuthorize("hasAuthority('wanxi:user:common')")
    public CommonResult addFabulous(@RequestParam Integer id){
        int result=englishWordService.addFabulous(id);
        return CommonResult.success(result);
    }

    /**
     * 英语单词批量导出到excel
     * @param list
     * @return
     */
    @PostMapping("/englishPort")
    @PreAuthorize("hasAuthority('wanxi:user:common')")
    public CommonResult englishPort(@RequestBody List<EnglishWordModel>list) throws Exception {
        List<WriteModel>list1=new ArrayList<>();
        for(int i=0;i<list.size();i++){
            WriteModel writeModel=new WriteModel();
            writeModel.setEnglish(list.get(i).getEnglish());
            writeModel.setChinese(list.get(i).getChinese());
            writeModel.setAuthor(list.get(i).getUsername());
            writeModel.setTime(list.get(i).getTimes());
            list1.add(writeModel);
        }
       int result= ExcelToole.writeExcell(list1);
        return CommonResult.success(result);
    }

    /**
     * 收藏单词展示
     * @param englishWordModel
     * @return
     */
    @PostMapping("/englishColectionShow")
    @PreAuthorize("hasAuthority('wanxi:user:common')")
    public CommonResult englishColectionShow(@RequestBody EnglishWordModel englishWordModel){
        List<EnglishWordModel>list=englishWordService.englishColectionShow(englishWordModel);
        return CommonResult.success(list);
    }

    /**
     * 用户取消收藏
     * @param id
     * @return
     */
    @PostMapping("/deleteCollect")
    @PreAuthorize("hasAuthority('wanxi:user:common')")
    public CommonResult deleteCollect(@RequestParam Integer id){
        int result=englishWordService.deleteCollect(id);
        return CommonResult.success(result);
    }
}
