package com.wanxi.springboot09mybatis.controller;


import com.wanxi.springboot09mybatis.comoen.CommonResult;
import com.wanxi.springboot09mybatis.model.*;
import com.wanxi.springboot09mybatis.service.UserRootService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/root")
public class UserRootController {

    @Resource
    UserRootService userRootService;
    /**
     * 冻结解冻账号
     * @return
     */
    @PostMapping("/frozen")
    @PreAuthorize("hasAuthority('wanxi:root:frozen')")
    public CommonResult frozenUser(@RequestParam Integer id,@RequestParam Integer status){
        int result = userRootService.frozenUser(id, status);
        return CommonResult.success(result);
    }
    /**
     * 查找所有用户
     * @param wxUser
     * @return
     */
    @PostMapping("/findSome")
    @PreAuthorize("hasAuthority('wanxi:root:defend_user_type')")
    public CommonResult findSome(@RequestBody WxUser wxUser){
        List<WxUser>list=userRootService.findSome(wxUser);
          return CommonResult.success(list);
    }

    /**
     * 管理员删除角色信息
     * @param id
     * @return
     */
    @PostMapping("/delete")
    @PreAuthorize("hasAuthority('wanxi:root:defend_user')")
    public CommonResult rootDeleteUser(@RequestParam Integer id){
        int result=userRootService.rootDeleteUser(id);
        return CommonResult.success(result);
    }

    /**
     * 根据id查找用户
     * @param id
     * @return
     */
    @GetMapping("/findCharacterById")
    @PreAuthorize("hasAuthority('wanxi:root:defend_user')")
    public CommonResult findCharacterById(@RequestParam Integer id){
        WxUser wxUser=userRootService.findCharacterById(id);
        return CommonResult.success(wxUser);
    }

    /**
     * 查找所有角色类型
     * @return
     */
    @GetMapping("/findCharacter")
    @PreAuthorize("hasAuthority('wanxi:root:defend_user_type')")
    public CommonResult findCharacter(){
       List<UserCharacter>list=userRootService.findCharacter();
       return CommonResult.success(list);
    }

    /**
     * 给复选框赋值
     * @param id
     * @return
     */
    @GetMapping("/setListCharacter")
    @PreAuthorize("hasAuthority('wanxi:root:defend_root_type')")
    public CommonResult setListCharacter(@RequestParam Integer id){
        List<UserCharacter>list=userRootService.setListCharacter(id);
        return CommonResult.success(list);
    }

    /**
     * 修改用户的角色
     * @param list
     * @return
     */
    @PostMapping("/editCharacter")
    @PreAuthorize("hasAuthority('wanxi:root:defend_user_type')")
    public CommonResult editCharacter(@RequestBody List<UserRole>list){
        int result=0;
        for(int i=0;i<list.size();i++){
            if(i==0) {
                userRootService.rootDeleteUserRole(list.get(i).getUserId());
            }
            result=userRootService.addUserRole(list.get(i).getRoleId(),list.get(i).getUserId());
            if(result!=1){
                break;
            }
        }
//        if(result!=1||result!=0){
//            return CommonResult.failed("修改失败");
//        }
//        else {
            return CommonResult.success(result);


    }

    /**
     * 展示角色以及对应的权限信息
     * @param userCharacter
     * @return
     */
    @PostMapping("/listCharacter")
    @PreAuthorize("hasAuthority('wanxi:root:defend_root_type')")
    public CommonResult listCharacter(@RequestBody UserCharacter userCharacter){
        List<UserCharacter>list=userRootService.listCharacter(userCharacter);
        return CommonResult.success(list);
    }

    /**
     * 角色对应展示所有权限
     * @param
     * @return
     */
    @RequestMapping("/listAuthoritie")
    @PreAuthorize("hasAuthority('wanxi:root:defend_root_type')")
    public CommonResult listAuthoritie(){
        List<Permission>list=userRootService.listAuthoritie();
        return CommonResult.success(list);
    }

    /**
     * 展示角色信息
     * @param id
     * @return
     */
    @RequestMapping("/setAuthroitie")
    @PreAuthorize("hasAuthority('wanxi:root:defend_root_type')")
    public CommonResult setAuthroitie(@RequestParam Integer id){
        UserCharacter userCharacter=userRootService.setAuthroitie(id);
        return CommonResult.success(userCharacter);
    }
    /**
     * 给权限复选框赋值
     */
    @RequestMapping("/setAuthrotitieValue")
    @PreAuthorize("hasAuthority('wanxi:root:defend_root_type')")
    public CommonResult setAuthrotitieValue(@RequestParam Integer id){
        List<RolePermission>list=userRootService.setAuthrotitieValue(id);
        return CommonResult.success(list);
    }

    @RequestMapping("/editAuthoritie")
    @PreAuthorize("hasAuthority('wanxi:root:defend_root_type')")
    public CommonResult editAuthoritie(@RequestBody List<RolePermission> list){
        int results=0;
       for (int i=0;i<list.size();i++){
           if(i==0){
               int result=userRootService.deleteAuthroitie(list.get(i).getRoleId());
           }
           results=userRootService.insertAuthroitie(list.get(i).getRoleId(),list.get(i).getPermissionId());
           if(results!=1) break;
       }
//        if(results!=1||results!=0){
//            return CommonResult.failed("修改失败");
//        }
//        else {
            return CommonResult.success(results);

    }
}
