package com.wanxi.springboot09mybatis.controller;

import com.wanxi.springboot09mybatis.comoen.CommonResult;
import com.wanxi.springboot09mybatis.dao.SignDao;
import com.wanxi.springboot09mybatis.model.GroupModel;
import com.wanxi.springboot09mybatis.model.SignModel;
import com.wanxi.springboot09mybatis.model.WxUser;
import com.wanxi.springboot09mybatis.service.SignService;
import com.wanxi.springboot09mybatis.service.UserRootService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import sun.misc.SignalHandler;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/sign")
public class SignController {
    @Autowired
    SignService signService;
    @Autowired
    UserRootService userRootService;

    /**
     * 查看所有学员情况
     * @param wxUser
     * @return
     */
    @PostMapping("/findAllUser")
    @PreAuthorize("hasAuthority('wanxi:user:common')")
    public CommonResult findAllUser(@RequestBody WxUser wxUser){
        List<WxUser>list=signService.findAllUser(wxUser);
        return CommonResult.success(list);
    }

    /**
     * 展示小组
     * @return
     */
    @RequestMapping("/findgroup")
    @PreAuthorize("hasAuthority('wanxi:user:common')")
    public CommonResult findgroup(){
        List<GroupModel>list=signService.findgroup();
        return CommonResult.success(list);
    }

    /**
     * 根据id查找用户
     * @param id
     * @return
     */
    @RequestMapping("/findUserById")
    @PreAuthorize("hasAuthority('wanxi:teacher:all')")
    public CommonResult findUserById(@RequestParam Integer id){
        WxUser wxUser=signService.findUserById(id);
        return CommonResult.success(wxUser);
    }

    /**
     * 根据id修改用户
     * @param wxUser
     * @return
     */
    @RequestMapping("/signUserEdit")
    @PreAuthorize("hasAuthority('wanxi:teacher:all')")
    public CommonResult signUserEdit(@RequestBody WxUser wxUser){
        int result=signService.signUserEdit(wxUser);
        return CommonResult.success(result);
    }

    /**
     * 查看所有小组
     */
    @RequestMapping("/findAllGroup")
    @PreAuthorize("hasAuthority('wanxi:user:common')")
    public CommonResult findAllGroup(@RequestBody GroupModel groupModel){
        List<GroupModel>list=signService.findAllGroup(groupModel);

        for(int i=0;i<list.size();i++){
            int count=signService.getCount(list.get(i).getCode());
            list.get(i).setCountTotl(count);
        }

        return CommonResult.success(list);
    }

    /**
     * 查找小组长以及成员
     * @return
     */
    @RequestMapping("/findLeader")
    @PreAuthorize("hasAuthority('wanxi:user:common')")
    public CommonResult findLeader(@RequestParam String code){
        List<WxUser>list=signService.findLeader(code);
        return CommonResult.success(list);
    }


    /**
     * 查找教练
     * @return
     */
    @RequestMapping("/findCoach")
    @PreAuthorize("hasAuthority('wanxi:user:common')")
    public CommonResult findCoach(){
        List<WxUser>list=signService.findCoach();
        return CommonResult.success(list);
    }

    /**
     * 根据id查找小组
     * @param id
     * @return
     */
    @RequestMapping("/findGroupsById")
    @PreAuthorize("hasAuthority('wanxi:teacher:all')")
    public CommonResult findGroupsById(@RequestParam Integer id){
        GroupModel groupModel=signService.findGroupsById(id);
        return CommonResult.success(groupModel);
    }

    /**
     * 查找用户的code
     */
    @RequestMapping("/findUserCode")
    @PreAuthorize("hasAuthority('wanxi:teacher:all')")
    public CommonResult findUserCode(@RequestParam String username){
        WxUser wxUser=signService.findUserCode(username);
        return CommonResult.success(wxUser);
    }

    /**
     * 查找之前组长的id
     * @param code
     * @return
     */
    @RequestMapping("/findUseroldId")
    @PreAuthorize("hasAuthority('wanxi:teacher:all')")
    public CommonResult findUseroldId(@RequestParam String code){
         WxUser wxUser= signService.findUseroldId(code);
         return CommonResult.success(wxUser);
    }


    /**
     * 修改小组信息,并且把现在组长设置为成员，新组长增加权限
     * @param groupModel
     * @return
     */
    @PostMapping("/groupEdits")
    @PreAuthorize("hasAuthority('wanxi:teacher:all')")
    public CommonResult groupEdits(@RequestBody GroupModel groupModel){
        int results=0;
        int result=signService.groupEdits(groupModel);
        WxUser wxUser=signService.findnowId(groupModel.getTeamLeaderCode());
        int nowId=wxUser.getId();
        if(nowId!=groupModel.getOlderId()){
            userRootService.rootDeleteUserRole(groupModel.getOlderId());
            userRootService.rootDeleteUserRole(nowId);
            results=userRootService.addUserRole(5,groupModel.getOlderId());
            results=userRootService.addUserRole(4,nowId);
        }
        return CommonResult.success(result);
    }

    /**
     * 查找用户根据名称
     * @param username
     * @return
     */
    @GetMapping("/findUserByName")
    @PreAuthorize("hasAuthority('wanxi:teacher:all')")
    public CommonResult findUserByName(@RequestParam String username){
        WxUser wxUser=signService.findUserByName(username);
        return CommonResult.success(wxUser);
    }

    /**
     * 新增小组
     * @param groupModel
     * @return
     */
    @PostMapping("/addGroup")
    @PreAuthorize("hasAuthority('wanxi:teacher:all')")
    public CommonResult addGroup(@RequestBody GroupModel groupModel){
        WxUser wxUser=signService.findnowId(groupModel.getTeamLeaderCode());
        int result=signService.addGroup(groupModel);
        if(result==1){
            userRootService.rootDeleteUserRole(wxUser.getId());
            userRootService.addUserRole(4,wxUser.getId());
        }
        return CommonResult.success(result);
    }

    /**
     * 查找成员，没分组的
     */
    @GetMapping("/findLeaderNoGroup")
    @PreAuthorize("hasAuthority('wanxi:teacher:all')")
    public CommonResult findLeaderNoGroup(){
        List<WxUser>list=signService.findLeaderNoGroup();
        return CommonResult.success(list);
    }

    /**
     * 查找组长的小组code
     * @param username
     * @return
     */
    @RequestMapping("/findLeaderCodes")
    @PreAuthorize("hasAuthority('wanxi:user:sign')")
    public CommonResult findLeaderCode(@RequestParam String username){
        WxUser wxUser=signService.findLeaderCode(username);
        return CommonResult.success(wxUser);
    }

    /**
     * 根据groupCode查找小组成员
     * @param groupCode
     * @return
     */
    @RequestMapping("/findGroupUsers")
    @PreAuthorize("hasAuthority('wanxi:user:sign')")
    public CommonResult findGroupUser(@RequestParam String groupCode){
        List<WxUser>list=signService.findGroupUser(groupCode);
        return CommonResult.success(list);
    }

    /**
     * 保存小组签到
     * @param list
     * @return
     */
    @PostMapping("/saveSign")
    @PreAuthorize("hasAuthority('wanxi:user:sign')")
    public CommonResult saveSign(@RequestBody List<SignModel> list){
        int result=0;
        for(int i=0;i<list.size();i++){
            String date= LocalDate.now().toString();
            list.get(i).setTime(date);
            result=signService.saveSign(list.get(i));
            if(result==0){
                break;
            }
            if(result==2){
                break;
            }
        }
        return CommonResult.success(result);
    }

    /**
     * 展示签到数据
     * @param signModel
     * @return
     */
    @PostMapping("/signShow")
    @PreAuthorize("hasAuthority('wanxi:user:sign')")
    public CommonResult signShow(@RequestBody SignModel signModel){
        List<SignModel>list=signService.signShow(signModel);
        return CommonResult.success(list);
    }

    /**
     * 教师删除签到数据
     */
    @PostMapping("/deleteAllSign")
    @PreAuthorize("hasAuthority('wanxi:teacher:all')")
    public CommonResult deleteAllSign(@RequestBody List<SignModel>list){
        int result=0;
        for(int i=0;i<list.size();i++){
            result=signService.deleteAllSign(list.get(i).getId());
           if(result==0){
               break;
           }
            }
        return CommonResult.success(result);
        }


}
