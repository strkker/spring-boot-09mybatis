package com.wanxi.springboot09mybatis.controller;
import com.wanxi.springboot09mybatis.comoen.CommonResult;
import com.wanxi.springboot09mybatis.model.WordModel;
import com.wanxi.springboot09mybatis.service.WordService;
import com.wanxi.springboot09mybatis.tool.WordTool;
import io.lettuce.core.dynamic.annotation.CommandNaming;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/word")
public class WordController {
    @Autowired
    WordService wordService;
    @Autowired
    WordTool wordTool;

    /**
     * 保存学员的word内容
     * @param wordModel
     * @return
     */
    @RequestMapping("/saveWord")
    @PreAuthorize("hasAuthority('wanxi:user:common')")
    public CommonResult saveWord(@RequestBody WordModel wordModel){
        int result=wordService.saveWord(wordModel);
        return CommonResult.success(result);
    }

    /**
     * 展示学员自身word内容
     * @param username
     * @return
     */
    @RequestMapping("/showSelfWord")
    @PreAuthorize("hasAuthority('wanxi:user:common')")
    public CommonResult showSelfWord(@RequestParam String username){
        List<WordModel>list=wordService.showSelfWord(username);
        return CommonResult.success(list);
    }

    /**
     * 根据id删除word
     * @param id
     * @return
     */
    @RequestMapping("/deleteWord")
    @PreAuthorize("hasAuthority('wanxi:user:common')")
    public CommonResult deleteWord(@RequestParam Integer id){
        int result=wordService.deleteWord(id);
       return CommonResult.success(result);
    }

    /**
     * 根据id查找word
     * @param id
     * @return
     */
    @RequestMapping("/findWordById")
    @PreAuthorize("hasAuthority('wanxi:user:common')")
    public CommonResult findWordById(@RequestParam Integer id){
        WordModel word=wordService.findWordById(id);
        return CommonResult.success(word);
    }

    /**
     * 学员修改word
     * @param wordModel
     * @return
     */
    @RequestMapping("/editWords")
    @PreAuthorize("hasAuthority('wanxi:user:common')")
    public CommonResult editWords(@RequestBody WordModel wordModel){
        int result=wordService.editWords(wordModel);
        return CommonResult.success(result);
    }

    /**
     * 学员导出word自己文件
     * @param wordModel
     * @return
     */
    @RequestMapping("/wordPost")
    @PreAuthorize("hasAuthority('wanxi:user:common')")
    public CommonResult wordPost(@RequestBody WordModel wordModel){
        String date= LocalDateTime.now().toString();
        wordModel.setTime(date);
        Map<String,Object>map=new HashMap<>();
        map.put("name",""+wordModel.getUsername()+"");
        map.put("time", ""+wordModel.getTime()+"");
        map.put("title",""+ wordModel.getTitle());
        map.put("harvest",""+wordModel.getHarvest()+"");
        map.put("Insufficient", ""+wordModel.getInsufficient()+"");
        map.put("type",""+wordModel.getType()+"");
        map.put("headmaster", ""+wordModel.getHeadmaster()+"");
        map.put("teacher",""+ wordModel.getTeacher()+"");
        map.put("help", ""+wordModel.getHeadmaster()+"");
        int result=wordTool.WordCreat(map);
        return CommonResult.success(result);
    }

    /**
     * 展示学员日报
     * @param wordModel
     * @return
     */
    @PostMapping("/showStuWord")
    @PreAuthorize("hasAuthority('wanxi:user:look_word')")
    public CommonResult showStuWord(@RequestBody WordModel wordModel){
        List<WordModel>list=wordService.showStuWord(wordModel);
        return CommonResult.success(list);
    }

    /**
     * 教练提交已查看
     * @param id
     * @return
     */
    @PostMapping("/wordLook")
    @PreAuthorize("hasAuthority('wanxi:user:ready_look')")
    public CommonResult wordLook(@RequestParam Integer id){
        int result=wordService.wordLook(id);
        return CommonResult.success(result);
    }
}
