package com.wanxi.springboot09mybatis.tool;

import com.alibaba.excel.EasyExcelFactory;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.metadata.Sheet;
import com.wanxi.springboot09mybatis.model.WriteModel;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public class ExcelToole {
    public static int writeExcell(List<WriteModel>list) throws Exception {
        OutputStream outputStream = new FileOutputStream("E:\\第9阶段录屏\\导出excel文件\\test.xlsx");
        ExcelWriter writer= EasyExcelFactory.getWriter(outputStream);
        Sheet sheet=new Sheet(1,0, WriteModel.class);
        sheet.setSheetName("第一个sheet文件");
        writer.write(list,sheet);
        writer.finish();
        outputStream.close();
        return 1;
    }
}
