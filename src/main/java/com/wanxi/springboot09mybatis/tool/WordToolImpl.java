package com.wanxi.springboot09mybatis.tool;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.Version;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.Map;
@Service
public class WordToolImpl implements WordTool {
    @Override
    public int WordCreat(Map<String, Object>dataMap) {
        {
//        Map<String,Object > dataMap = new HashMap();
            try {
//                dataMap.put("name","name");
//                dataMap.put("time", "name");
//                dataMap.put("title", "name");
//                dataMap.put("harvest","name");
//                dataMap.put("Insufficient", "name");
//                dataMap.put("type", "name");
//                dataMap.put("headmaster","name");
//                dataMap.put("teacher", "name");
//                dataMap.put("help","name");
                //Configuration 用于读取ftl文件
                Configuration configuration = new Configuration(new Version("2.3.0"));
                configuration.setDefaultEncoding("utf-8");

                /**
                 * 指定ftl文件所在目录的路径，而不是ftl文件的路径
                 */
                //我的路径是F：/idea_demo/日报.ftl
                configuration.setDirectoryForTemplateLoading(new File("E:\\第9阶段录屏"));

                //输出文档路径及名称
                File outFile = new File("E:\\第9阶段录屏\\导出word文件\\工作日报.doc");

                //以utf-8的编码读取ftl文件
                Template template = configuration.getTemplate("工作日报.ftl", "utf-8");
                Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile), "utf-8"), 10240);
                template.process(dataMap, out);
                out.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return 1;
        }
    }
}
