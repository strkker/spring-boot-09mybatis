package com.wanxi.springboot09mybatis.dao;

import com.wanxi.springboot09mybatis.model.GroupModel;
import com.wanxi.springboot09mybatis.model.SignModel;
import com.wanxi.springboot09mybatis.model.WxUser;

import java.util.List;

public interface SignDao {
    List<WxUser> findAllUser(WxUser wxUser);

    List<GroupModel> findgroup();

    WxUser findUserById(Integer id);

    int signUserEdit(WxUser wxUser);

    List<GroupModel> findAllGroup(GroupModel groupModel);

    int getCount(String code);

    List<WxUser> findLeader(String code);

    List<WxUser> findCoach();

    GroupModel findGroupsById(Integer id);

    WxUser findUserCode(String username);

    int groupEdits(GroupModel groupModel);

    WxUser findnowId(String teamLeaderCode);

    WxUser findUseroldId(String teamLeaderCode);

    WxUser findUserByName(String username);

    GroupModel isExistGroup(GroupModel groupModel);

    int addGroup(GroupModel groupModel);

    List<WxUser> findLeaderNoGroup();

    WxUser findLeaderCode(String username);

    List<WxUser> findGroupUser(String groupCode);

    int saveSign(SignModel signModel);

    SignModel isSaveSign(SignModel signModel);

    List<SignModel> signShow(SignModel signModel);

    int deleteAllSign(int id);
}
