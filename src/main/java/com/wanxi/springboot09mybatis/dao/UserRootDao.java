package com.wanxi.springboot09mybatis.dao;

import com.wanxi.springboot09mybatis.comoen.CommonResult;
import com.wanxi.springboot09mybatis.model.*;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserRootDao {
    /**
     * 冻结解冻账号
     * @param id
     * @return
     */
    int frozenUser(@Param("id") Integer id,@Param("status") Integer status);

    List<WxUser> findSome(WxUser wxUser);

    int rootDeleteUser(Integer id);

    WxUser findCharacterById(Integer id);

    List<UserCharacter> findCharacter();

    List<UserCharacter> setListCharacter(Integer id);

    void rootDeleteUserRole(int userId);

    int addUserRole(int roleId,int userId);

    List<UserCharacter> listCharacter(UserCharacter userCharacter);

    List<Permission> listAuthoritie();

    UserCharacter setAuthroitie(Integer id);

    List<RolePermission> setAuthrotitieValue(Integer id);

    int deleteAuthroitie(int roleId);

    int insertAuthroitie(int roleId, int permissionId);
}
