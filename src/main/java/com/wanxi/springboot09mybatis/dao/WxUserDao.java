package com.wanxi.springboot09mybatis.dao;

import com.wanxi.springboot09mybatis.model.LoginParams;
import com.wanxi.springboot09mybatis.model.Permission;
import com.wanxi.springboot09mybatis.model.WxUser;

import java.util.List;

public interface WxUserDao {
    List<WxUser> getUserByName(String name);
    List<Permission> getPermisionByUserId(Integer userId);

    int saveFailedLogin(LoginParams loginPrams);

    List<LoginParams> isAreadLoginThreeFail(String username);

    void deleteBeforDayLogin();

    WxUser isHaveUser(WxUser wxUser);

    int addUser(WxUser wxUser);
}
