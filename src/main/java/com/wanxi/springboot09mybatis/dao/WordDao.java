package com.wanxi.springboot09mybatis.dao;

import com.wanxi.springboot09mybatis.model.WordModel;

import java.util.List;

public interface WordDao {
    int saveWord(WordModel wordModel);

    List<WordModel> showSelfWord(String username);

    int deleteWord(Integer id);

    WordModel findWordById(Integer id);

    int editWords(WordModel wordModel);

    List<WordModel> showStuWord(WordModel wordModel);

    int wordLook(Integer id);
}
