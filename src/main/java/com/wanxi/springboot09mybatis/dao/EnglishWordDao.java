package com.wanxi.springboot09mybatis.dao;

import com.wanxi.springboot09mybatis.model.EnglishWordModel;

import java.util.List;

public interface EnglishWordDao {
    EnglishWordModel isExitEnglishWord(EnglishWordModel englishWordModel);
    int saveEnglish(EnglishWordModel englishWordModel);

    List<EnglishWordModel> findEnglishWord(EnglishWordModel englishWordModel);

    EnglishWordModel findEnglishWordById(Integer id);

    EnglishWordModel isHascollectEnglishWord(EnglishWordModel englishWordModel);

    int collectEnglishWord(EnglishWordModel englishWordModel);

    EnglishWordModel ishasEnglishFabulous(EnglishWordModel englishWordModel);

    int englishFabulous(EnglishWordModel englishWordModel);

    int addFabulous(Integer id);

    List<EnglishWordModel> englishColectionShow(EnglishWordModel englishWordModel);

    int deleteCollect(Integer id);
}
