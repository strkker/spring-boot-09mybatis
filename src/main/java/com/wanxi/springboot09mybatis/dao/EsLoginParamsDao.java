package com.wanxi.springboot09mybatis.dao;

import com.wanxi.springboot09mybatis.model.LoginParamsEs;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


public interface EsLoginParamsDao extends ElasticsearchRepository<LoginParamsEs,Integer> {

}
