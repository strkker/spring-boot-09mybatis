package com.wanxi.springboot09mybatis;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan(basePackages = "com.wanxi.springboot09mybatis.dao")
public class SpringBoot09mybatisApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBoot09mybatisApplication.class, args);
    }

}
