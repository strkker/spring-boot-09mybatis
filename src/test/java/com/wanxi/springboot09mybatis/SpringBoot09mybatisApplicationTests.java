package com.wanxi.springboot09mybatis;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.net.InetAddress;
import java.net.UnknownHostException;

@SpringBootTest
class SpringBoot09mybatisApplicationTests {
    @Autowired
    ApplicationContext applicationContext;

    @Autowired
    PasswordEncoder passwordEncoder;
    @Test
    void contextLoads() {
     String a=passwordEncoder.encode("root_002");
        System.out.println(a);
    }
}
